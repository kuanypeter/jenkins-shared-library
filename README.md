### Demo Project:
Create a Jenkins Shared Library
- has own repo
- extension to pipeline
- references shared logic in jenkinsfile.

### Technologies used:
Jenkins, Groovy, Docker, Git, Java, Maven
### Project Description:
Create a Jenkins Shared Library to extract common build
logic:
- Create separate Git repository for Jenkins Shared Library project
- Create functions in the JSL to use in the Jenkins pipeline
   - create functions in __vars folder__
   - Create helper code in __src folder__
   - create non-groovy files in __resources folder__
   - Extract common functionality to src folder
- Integrate and use the JSL in Jenkins Pipeline (globally and for a specific project in Jenkinsfile)
   - In Jenkins UI -> __Manage Jenkins -> System -> Global pipeline libraries__
    - Add Jenkins shared library
    - give it a __Name__
    - configure __Default version__
    - Retrieval method: __Modern SCM__
    - Configure git repo and its credentials
   - Create a jenkins shared library branch in jenkins-java-maven project
     - configure jenkinsfile to use jenkins shared library import at the top of the file
        @Library("Jenkins-shared-library-name") then refer functions directly e.g buildJar()
- To add Jenkins shared library to only one project, add the code below at the top of its jenkinsfile
   library identifier: 'jenkins-shared-library@main', retriever: modernSCM(
      [
        $class: 'GitSCMSource',
        remote: 'https://gitlab.com/08-jenkins/jenkins-shared-library.git',
        credentialsId: 'gitlab-credentials-id'
      ]
    )